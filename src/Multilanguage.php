<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   19.09.2017
 */

namespace alexs\yii2multilanguage;
use Yii;
use yii\base\BaseObject;
use yii\web\Cookie;

class Multilanguage extends BaseObject
{
    public $languages;
    public $rules;
    public $current_language = NULL;
    // the name of parameter for routes, session and cookie
    public $param_name = 'language';
    // use the cookie and session
    public $cookie_enabled = true;
    public $cookie_lifetime = 2592000; // 1 month

    public function init() {
        parent::init();
        $this->setDefault();
    }
   
    /**
     * @return string
     */
    public function getCurrent() {
        if ($this->current_language !== NULL) {
            return $this->current_language;
        }
        if (!$this->cookie_enabled) {
            return $this->getFirst();
        }
        $language_id = $this->getFromCookie();
        return $this->isAllowed($language_id) ? $language_id : $this->getFirst();
    }

    /**
     * @param string $language_id
     * @return void
     */
    public function setCurrent($language_id) {
        if (!$this->isAllowed($language_id) || ($this->current_language === $language_id)) {
            return;
        }
        $this->current_language = $language_id;
        Yii::$app->language = $this->getCurrentIETF();
        if ($this->cookie_enabled) {
            $this->saveToCookie($language_id);
        }
    }

    /**
     * @return string
     */
    public function getCurrentIETF() {
        return key($this->languages[$this->getCurrent()]);
    }

    /**
     * @return string
     */
    public function getFirst() {
        return key($this->languages);
    }

    /**
     * @param string $language_id
     * @return bool
     */
    public function isAllowed($language_id) {
        return ($language_id !== NULL) && array_key_exists($language_id, $this->languages);
    }

    /**
     * @return void
     */
    protected function setDefault() {
        $language_id = $this->getFromCookie();
        $this->setCurrent($this->isAllowed($language_id) ? $language_id : $this->getFirst());
    }
    
    /**
     * @return string|NULL
     */
    protected function getFromCookie() {
        if (!$language_id = Yii::$app->session->get($this->param_name)) {
            if ($Cookie = Yii::$app->request->cookies->get($this->param_name)) {
                $language_id = $Cookie->value;
            }
        }
        return $language_id;
    }

    /**
     * @param string $language_id
     * @return void
     */
    protected function saveToCookie($language_id) {
        Yii::$app->session->set($this->param_name, $language_id);
        Yii::$app->response->cookies->add(new Cookie([
            'name'  =>$this->param_name,
            'value' =>$language_id,
            'expire'=>time() + $this->cookie_lifetime,
        ]));
    }
}
