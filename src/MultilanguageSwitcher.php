<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   03.09.2017
 */

namespace alexs\yii2multilanguage;
use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;

class MultilanguageSwitcher extends Widget
{
    public $view;

    public function init() {
        parent::init();
        if (!Yii::$app->multilanguage instanceof Multilanguage) {
            throw new InvalidConfigException('The multilanguage component is not initialized');
        }
    }

    public function run() {
        return $this->render($this->view, [
            'current'  =>Yii::$app->language,
            'languages'=>Yii::$app->multilanguage->languages,
        ]);
    }
}
