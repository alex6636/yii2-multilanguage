<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   19-Sep-17
 */

namespace alexs\yii2multilanguage;
use Yii;
use yii\base\BootstrapInterface;
use yii\web\Application;
use yii\base\Controller;
use yii\base\Event;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app) {
        $app->on(Application::EVENT_BEFORE_REQUEST, function() use ($app) {
            /** @var Multilanguage|null $multilanguage */
            $multilanguage = Yii::$app->get('multilanguage', false);
            if ($multilanguage instanceof Multilanguage) {
                if (!empty($multilanguage->rules)) {
                    $app->getUrlManager()->addRules($multilanguage->rules);
                }   
            }
        });

        Event::on(Controller::className(), Controller::EVENT_BEFORE_ACTION, function($event) {
            /** @var Multilanguage|null $multilanguage */
            $multilanguage = Yii::$app->get('multilanguage', false);
            if ($multilanguage instanceof Multilanguage) {
                $multilanguage->setCurrent(Yii::$app->request->get($multilanguage->param_name));   
            }
        });
    }
}
