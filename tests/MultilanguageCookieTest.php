<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   20.09.2017
 */

namespace alexs\yii2multilanguage\tests;
use alexs\yii2phpunittestcase\TestCase;
use Yii;
use yii\web\Application;

abstract class MultilanguageCookieTest extends TestCase
{
    public function testGetDefault() {
        /** @var Multilanguage $multilanguage */
        $multilanguage = Yii::$app->multilanguage;
        $this->assertEquals('ru', $multilanguage->getCurrent());
        $this->assertEquals('ru-RU', $multilanguage->getCurrentIETF());
        $this->assertEquals('ru-RU', Yii::$app->language);
    }

    public function testSetGet() {
        /** @var Multilanguage $multilanguage */
        $multilanguage = Yii::$app->multilanguage;

        $multilanguage->setCurrent('ru');
        $this->assertEquals('ru', $multilanguage->getCurrent());
        $this->assertEquals('ru-RU', $multilanguage->getCurrentIETF());
        $this->assertEquals('ru-RU', Yii::$app->language);

        $multilanguage->setCurrent('en');
        $this->assertEquals('en', $multilanguage->getCurrent());
        $this->assertEquals('en-US', $multilanguage->getCurrentIETF());
        $this->assertEquals('en-US', Yii::$app->language);
    }
    
    abstract protected function getComponentConfig();
    
    protected function mockApplication() {
        new Application([
            'id'=>'test_app',
            'basePath'=>__DIR__,
            'vendorPath'=>dirname(__DIR__) . '/vendor',
            'components'=>[
                'multilanguage'=>[
                    'class'=>'alexs\yii2multilanguage\tests\Multilanguage',
                    'languages'=>[
                        // the first language will be used by default
                        //'id'=>['IETF' =>'Title']
                        'ru'=>['ru-RU'=>'Русский'],
                        'en'=>['en-US'=>'English'],
                    ],
                    'rules'=>[
                        '<language:[a-z]{2}>'=>'index/index',
                        '<language:[a-z]{2}>/<controller>'=>'<controller>',
                        '<language:[a-z]{2}>/<controller>/<action>'=>'<controller>/<action>',
                    ],
                    'cookie_enabled'=>false,
                ],
            ],
        ]);
    }
}
