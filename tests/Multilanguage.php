<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   20.09.2017
 */

namespace alexs\yii2multilanguage\tests;

class Multilanguage extends \alexs\yii2multilanguage\Multilanguage
{
    public $cookie_current_language;

    /**
     * @return string|NULL
     */
    protected function getFromCookie() {
        return $this->cookie_current_language;
    }

    /**
     * @param string $language_id
     * @return void
     */
    protected function saveToCookie($language_id) {
        $this->cookie_current_language = $language_id;
    }
}