<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   19.09.2017
 */

namespace alexs\yii2multilanguage\tests;
use alexs\yii2phpunittestcase\TestCase;
use yii\web\Application;

class MultilanguageTest extends TestCase
{
    public function testGetFirst() {
        /** @var Multilanguage $multilanguage */
        $multilanguage = \Yii::$app->multilanguage;
        $this->assertEquals('ru', $multilanguage->getFirst());
    }

    public function testIsAllowed() {
        /** @var Multilanguage $multilanguage */
        $multilanguage = \Yii::$app->multilanguage;
        $this->assertFalse($multilanguage->isAllowed(NULL));
        $this->assertFalse($multilanguage->isAllowed('invalid'));
        $this->assertTrue($multilanguage->isAllowed('ru'));
        $this->assertTrue($multilanguage->isAllowed('en'));
    }

    protected function mockApplication() {
        new Application([
            'id'=>'test_app',
            'basePath'=>__DIR__,
            'vendorPath'=>dirname(__DIR__) . '/vendor',
            'components'=>[
                'multilanguage'=>[
                    'class'=>'alexs\yii2multilanguage\tests\Multilanguage',
                    'languages'=>[
                        // the first language will be used by default
                        //'id'=>['IETF' =>'Title']
                        'ru'=>['ru-RU'=>'Русский'],
                        'en'=>['en-US'=>'English'],
                    ],
                    'rules'=>[
                        '<language:[a-z]{2}>'=>'index/index',
                        '<language:[a-z]{2}>/<controller>'=>'<controller>',
                        '<language:[a-z]{2}>/<controller>/<action>'=>'<controller>/<action>',
                    ],
                ],
            ],
        ]);
    }
}
