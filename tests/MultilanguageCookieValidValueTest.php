<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   20.09.2017
 */

namespace alexs\yii2multilanguage\tests;

class MultilanguageCookieValidValueTest extends MultilanguageCookieTest
{
    protected function getComponentConfig() {
        return [
            'class'=>'alexs\yii2multilanguage\tests\Multilanguage',
            'languages'=>[
                // the first language will be used by default
                //'id'=>['IETF' =>'Title']
                'ru'=>['ru-RU'=>'Русский'],
                'en'=>['en-US'=>'English'],
            ],
            'rules'=>[
                '<language:[a-z]{2}>'=>'index/index',
                '<language:[a-z]{2}>/<controller>'=>'<controller>',
                '<language:[a-z]{2}>/<controller>/<action>'=>'<controller>/<action>',
            ],
            'cookie_current_language'=>'en',
        ];
    }
}
